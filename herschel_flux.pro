;Herschel flux code

pro herschel_flux

file = '/home/Aaron/External/External/Herschel/combined_map_R_HP30_DF30_samp6.4.fits' ; load file
image=mrdfits(file, 1, header) ; input image into array
;image_scale = abs(fxpar(header, "CRDELT2")) ; determine the image scale from header


set_xy ; reset the xy coordinates

steradians_per_sqarcsec = ( !pi / 180. / 3600. )^2.0 ; conversion

ra = 192.2045708 ; object ra in decimal degrees
dec = -41.310538 ; object dec in decimal degrees
radius =  4.4        ; in pixels
min_back = 1.5*radius   ; in pixels
max_back = 2.0*radius       ; in pixels

; convert ra dec to x,y
adxy,header,ra,dec,x0,y0

; make xy arrays xx and yy
  ss = size(image)
  x=findgen(ss(1))
  y=findgen(ss(2))
  xx = x # (y*0.0+1.0)
  yy = (x*0.0+1.0) # y  
  siz = size(xx)
  x0 = x0[0]
  y0= y0[0]
  x0 = make_array(siz(1),siz(2), value = x0)
  y0 = make_array(siz(1),siz(2), value = y0)
  
  rr = ( (xx-x0)^2 + (yy-y0)^2 )^.5
  rw = where(rr le radius, npixels)
 
flux = total(image(rw)) ; total flux before background subtraction

r_annulus = where ( (rr gt min_back) * (rr le max_back), nback)
back = image(r_annulus)

; filter the background
    nback=float(n_elements(back))
    ave_back=mean(back)
    med_back=median(back)
    sig_back=stddev(back)

histogauss, back, hist
  hist_height = hist[0] ; height of the gaussian 
  hist_mean = hist[1]   ; mean of the gaussian
  hist_sd = hist[2]     ; standard deviation of the gaussian
  hist_95 = hist[3]    ; the half-width of the 95% conf. interval of the standard mean
  hist_norm = hist[4]  ; 1/(N-1)*total( (y-mean)/sigma)^2 ) = a measure of normality 
  diff = (hist_mean-med_back)/hist_mean

npix = float(n_elements(rw))
  back = hist_mean & errback=hist_sd

netflux = total( (image(rw)-back) ) ; background fluxes

print, "Net Flux:", netflux, " Jy"
print, "Total Flux:", flux, " Jy"
print, "Background value:", back, " Jy"

stop
end