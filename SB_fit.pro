;mpfit function
function SB_MODEL, X, P
common params, old_interp, flux_interp

return, (P[0]*flux_interp + P[1]*old_interp)
end

;***********************************
; fit to the SB K models (2007)
pro SB_fits
common fun_cod, old
common bcg, bcg
common params, old_interp, flux_interp
;lambda_bcg = bcg.wave
;flux_bcg = bcg.flux
;flux_err = bcg.errflux
;z = bcg.z
plotmod=make_array(5,318)

; restore all of the models in the model variables
restore, "/home/Aaron/Desktop/Megan/analysis/Modelling/models.sav"
;restore, "/home/Aaron/Desktop/Megan/analysis/Modelling/Oldstardata.sav"

;DO NOT USE OLD STAR DATA MODELS!

restore,"/home/Aaron/Desktop/Megan/analysis/IDL_Code/elltemp.save"
ell = read_ascii("/home/Aaron/Desktop/Megan/analysis/IDL_Code/OldElliptical.spectrum1.txt",template=elltemp)
; choose the oldest model
ellages=ell.age
ellwave=ell.wave_angstrom
elllogflux=ell.logflux

;convert from F_lambda to F_nu
dlam_dHz = alog10(3*10^18./(3*10^18./ellwave)^2)
elllogflux = elllogflux + dlam_dHz 

; convert ellwave to microns

ellwave= ellwave/1.e4


; find the oldest model
wage_high=where (ell.age eq max(ell.age)) 
print,"Max old star age is: ", max(ell.age)
ellwave=ellwave(wage_high)
ellflux=elllogflux(wage_high)


old = {flux: dblarr(1221), wave: dblarr(1221)}

old.wave = ellwave 
old.flux = 10^(ellflux)
old.flux = old.flux/1e27

print, max(old.flux), old.wave(where(old.flux eq max(old.flux)))


ra=160.18526 

dec=39.95292

twomass = two_mass(ra, dec)

z=0.137500
distan = lumdist(z)
xhers = [70, 100, 160, 250, 350, 500]
yhers = [542, 757, 769, 376, 135, 56]
errhers = [6, 6, 4, 6, 6, 8]
lambda_bcg = [1.235, 1.662, 2.159, 3.6, 4.5, 5.8, 8.0, 24., 70.]
flux_bcg = [3.8, 5.31 , 5.533 ,3.15, 2.77, 3.4, 8.5, 74.8, 891.]
;percenterr = [twomass.jerr(0)/twomass.j(0),twomass.herr(0)/twomass.h(0) , twomass.kerr(0)/twomass.k(0),0.05, 0.05, 0.05, 0.05, .10, .20]
percenterr = [0.05, 0.05, 0.05,0.05, 0.05, 0.05, 0.05, .10, .20]


yerr = flux_bcg * percenterr
flux_err = yerr

l=0
j = 0
SBmodel = make_array(7221,318)
ospmodel = make_array(7221,1221)
chisq = fltarr(7221)
res0 = chisq
res1 = chisq
n = size(lambda_bcg)

; estimate the parameter for the old stellar population
old_interp = interpol(old.flux[*], old.wave, lambda_bcg, /spline)



for j=0, n(1)-1 do begin
  if (long(lambda_bcg(j)*(1+z)) gt 2) then begin
    frac = flux_bcg(j)/old_interp(j)
    j = 50
  endif
endfor

parinfo=replicate({value:0.D, fixed:0, limited:[0,0], limits:[0.D,0], mpprint:0 },2)
parinfo(0).limited = 1
parinfo(1).limited = 1
 

;loop through all SB models
while j lt 7220 do begin 

flux_model = modelflux[j, *]

;make a first guess at the flux match to the 70 micron file ; NOT MATCHED CAN ONLY SCALE TO DISTANCE
flux_interp = interpol( flux_model, wave_rest, lambda_bcg, /spline) ;model flux at the correct wavelengths

;for i=0, n(1)-1 do begin
;  if (long(lambda_bcg(i)*(1.+z)) gt 50) then begin
;    flux = flux_bcg(i) ; 70 micron flux
;    start = flux/flux_interp(i)
;    i=100
;  endif 
;endfor

start = 1000*(50./distan)^(2.0) ; Only scaled by the distance! and convert to mJy

parinfo(0).value=start
parinfo(0).limits = [start*.8, start*1.2]
parinfo(1).value = frac
parinfo(1).limits = [frac*.8, frac*1.2]

result=mpfitfun('SB_MODEL', lambda_bcg, flux_bcg, flux_err, PARINFO = parinfo, PERROR=error) 

chisq(l) = result(2)
 
sbmodel[l, *] = result(0) * modelflux[j,*]
ospmodel[l,*] = result(1) * old.flux[*]
res0(l) = result(0)
res1(l) = result(1)


l=l+1
j = j+1
endwhile

close, 8


sbmod = sbmodel[0:l-1,*]
ospmod =  ospmodel[0:l-1,*]


chisq = chisq(0:l-1)

min_pos = where(abs(chisq-1) eq min(abs(chisq-1)))
min_pos = 7024
min_chisq = chisq(min_pos)
min_model = modelnames(min_pos)

min_sb = sbmod(min_pos,*)
min_osp = ospmod(min_pos,*)

print, res0(min_pos), res1(min_pos)



; print near models to a file to determine their properties


bestmodelfile = '/home/Aaron/Documents/abell1068.txt'
close, 10
openw, 10, bestmodelfile

  printf, 10, min_model, min_chisq ; here is the best model
  ; figure out the best models by looking at chisq within 10% of the best model
  
  for i = 0, l - 1 do begin
    if (abs((chisq(i)-min_chisq)/min_chisq) lt .1) then printf, 10, modelnames(i), chisq(i)
  endfor

osp_inter = interpol(min_osp, old.wave, wave_rest)
totalmod = osp_inter + min_sb[*]

; Make a vector of 16 points, A[i] = 2pi/16:
A = FINDGEN(17) * (!PI*2/16.)
; Define the symbol to be a unit circle with 16 points, 
; and set the filled flag:
USERSYM, COS(A)/3, SIN(A)/3, /FILL


xtitle = TeXtoIDL("\lambda [\mum]")
ytitle = TeXtoIDL(" F_{\nu} [mJy]")
title = "Infrared Emission from Abell 1068"
set_plot, 'ps'
showfont, 6, 'Complex Roman', /encapsulated
device, filename='/home/Aaron/Documents/Abell_1068.eps'

;power_bcg = 3.0*10^14/lambda_bcg*flux_bcg/1000. 

; plot HERSCHEL NUMBERS

xhers = [70, 100, 160, 250, 350, 500]
yhers = [542, 757, 769, 376, 135, 56]
errhers = [6, 6, 4, 6, 6, 8]

plot, lambda_bcg, flux_bcg, xtitle=xtitle, ytitle=ytitle, psym = 8 , xlog = 1, ylog =1, $
  xrange=[1,1000], yrange=[1,1000], title = title  
;oplot, lambda_bcg, power_bcg, psym=8
errplot, lambda_bcg, flux_bcg - yerr, flux_bcg+yerr
oploterr, xhers, yhers, errhers
oplot, wave_rest, min_sb , LINESTYLE = 1
oplot, old.wave, min_osp[*], LINESTYLE = 2
oplot, wave_rest, totalmod, LINESTYLE = 0
device, /close

k=0
totmag=0
for k=0, 317 do begin

if wave_rest(k) ge 8 and wave_rest(k) le 1000 then totmag = totmag + (3.*10^10)/wave_rest(k)*totalmod(k)
 


endfor 

print, totmag
print, total(totalmod*3*10^10./wave_rest), "total"
print, total(min_osp[*]*3*10^10./old.wave), "old"
print, total(min_sb*3*10^10./wave_rest), "sb"
stop

best_model = {flux: totalmod, wave: wave_rest} 

;return, best_model

end
