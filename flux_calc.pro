;***********************************
; spitzer fluxes
; make a circular aperture
; sum up flux
; sum up pixels
; report flux

; filelist should be a file with a list of full path names of mosaics
; SPITZER fits images, in wavelength order
; ra, dec in decimal J2000 degrees

pro flux_calc, fillname

; determine which bcg it is by RA and Dec
; once you know which bcg you can get the structure for that bcg which includes important data
; open list of files and pick the file and associated uncertainty file

file = strarr(2000) & uncfile=file & covfile=file ; find uncertainty file from same folder
flux = 0.0
line=""
exptime = 0.0 ; get this value from separate file
netflux=flux & netcflux=flux
errflux=flux
plterr=errflux & errfrac=errflux
foo=1.
COG_flux = dblarr(10)

;connect txAOR with RA and DEC
;aor_file = 'C:\Users\Aaron\Documents\MSU_Research\megan\analysis\files\AOR_RA_Dec.txt'
aor_file = '/home/Aaron/Desktop/Megan/analysis/files/AOR_RA_Dec.txt' 
d = ''
c = 0.0
b = 0.0
e = 0.0
aor_test = {name: strarr(552), ra: dblarr(552), dec: dblarr(552), aor: dblarr(552)} 
close, 1
openr, 1, aor_file

k=0
while ~ EOF(1) do begin
readf, 1, a, b, c, d
aor_test.name(k) = d
aor_test.ra(k) = b
aor_test.dec(k) = c
aor_test.aor(k) = a
k=k+1
endwhile  
close, 1

filename = "/home/Aaron/Dropbox/Megan/SpitzerGALEX_paper/2MASS_RA_Dec.csv"

a = ""
name = strarr(1000)
ra = dblarr(1000)
dec = dblarr(1000)


h=0
close, 1
openr, 1, filename 
while ~ EOF(1) do begin
readf, 1, a
lin = strsplit(a, ',', /EXTRACT)
name(h) = lin(0)
ra(h) = lin(1)
dec(h) = lin(2)
h=h+1

endwhile 
close, 1



;put bcg list into save file: name, redshift, RA, Dec from ken's cluster information  
;restore, "C:\Users\Aaron\Documents\MSU_Research\megan\analysis\files\BCGlocation.sav"
;restore, "/home/Aaron/Desktop/Megan/analysis/files/BCGlocation.sav"
;bcg_name=cwap.field1
;ra=cwap.field4
;dec=cwap.field5

; get ken's xray cluster names, RA, Dec, and redshift to match
;xray_list = "G:\kenxraylist.txt"
xray_list = "/home/Aaron/Documents/kenxraylist_edit.csv"
a = 0.0
xray = {name: strarr(241), ra: dblarr(241), dec: dblarr(241), redshift: dblarr(241), kpcasec: dblarr(241)} 
close, 13
openr, 13, xray_list
m=0
while ~ EOF(13) do begin 
readf, 13, a, b, c, e, d
xray.ra(m) = a
xray.dec(m) = b
xray.redshift(m)= c
xray.kpcasec(m) = e
xray.name(m) = d
m = m+1
endwhile
close, 13

;2MASS apertures: 5, 7, 10, 15, 20, 25, 30, 40, 50, 60
radius_list = double([5, 7, 10, 15, 20, 25, 30, 40, 50, 60, 70, 80])
;with some of the MIPS files you have two maic.fits for each, need to skip these
;filelist_maic = "C:\Users\Aaron\Documents\MSU_Research\megan\analysis\files\maic.txt"
filelist_maic = "/home/Aaron/Desktop/Megan/analysis/files/maic-linux.txt"

;filelist_munc = "C:\Users\Aaron\Documents\MSU_Research\megan\analysis\files\munc.txt"
filelist_munc = "/home/Aaron/Desktop/Megan/analysis/files/munc-linux.txt"

;filelist_mcov = "C:\Users\Aaron\Documents\MSU_Research\megan\analysis\files\mcov.txt"
filelist_mcov = "/home/Aaron/Desktop/Megan/analysis/files/mcov-linux.txt"

;eventually run through the filtered files for MIPS data
;filelist_mfilt = "C:\Users\Aaron\Documents\MSU_Research\megan\analysis\files\mfilt.txt"
;filelist_maic = "/home/Aaron/Desktop/Megan/analysis/files/mfilt-linux.txt"
;filelist_mfunc = "C:\Users\Aaron\Documents\MSU_Research\megan\analysis\files\mfunc.txt"
;filelist_munc = "/home/Aaron/Desktop/Megan/analysis/files/mfunc-linux.txt"

;filelist_mcov = "/home/Aaron/Desktop/Megan/analysis/files/mfcov-linux.txt"

;read in the normal image files
close, 1
openr, 1, filelist_maic

i=0

while ~ EOF(1) do begin  

readf, 1, line
file(i) = strcompress(line,/REMOVE_ALL)

i=i+1
endwhile 
close, 1

;read in the normal uncertainty files
close, 2
openr, 2, filelist_munc

j=0
while ~ EOF(2) do begin

readf, 2, line
uncfile(j) = line

j=j+1
endwhile
close, 2

;read in the coverage (exposure time) maps
close, 3
openr, 3, filelist_mcov

j=0
while ~ EOF(3) do begin

readf, 3, line
covfile(j) = line

j=j+1
endwhile
close, 3

bcg_num = i-1

chan_0 = 0
aor_0 = 0

prev_obs = {aor: 0.D, inst: '', chan: 0} 

;close, 41
;openw, 41, '/home/Aaron/MIPSexp.txt'

mipsaor = dblarr(232)

counter = 0
close, 41
openr, 41, "/home/Aaron/External/External/mipsAOR.txt"
while~EOF(41) do begin
readf, 41, qq
mipsaor(counter) = qq
counter = counter+1

endwhile






for i=0, 1 do begin
  file(i) = fillname
  header = headfits(file(i))

 if header[0] eq -1 then continue

  instrument = fxpar(header, 'INSTRUME')
  instrument = strcompress(instrument, /REMOVE_ALL)
  AORKEY = fxpar(header, "AORKEY")
  aorcheck = where(mipsaor eq AORKEY)
  ;if aorcheck(0) eq -1 then continue
  ;if  instrument eq 'IRAC' then continue
  ;covimage=mrdfits(covfile(i), 0, covheader)
  ;frames = max(covimage)
  ;exptime = frames * fxpar(header, "EXPTIME")
  ;line = string(fxpar(header, "AORKEY"))+ ' ' + string(exptime)+ ' '+ string(fxpar(header,'CHNLNUM'))
  ;printf, 41, line 

   ;continue
  ;file(i) = fillname
  
  image=mrdfits(file(i), 0, header)
  
  if image[0] eq 0 then continue
  
; get header information to use in database
  OBJECT = fxpar(header, "OBJECT") ; name of object
  OBJECT = strcompress(OBJECT,/REMOVE_ALL)
  INSTRUMENT = fxpar(header, "INSTRUME") ; either MIPS or IRAC
  INSTRUMENT = strcompress(INSTRUMENT,/REMOVE_ALL)
  CHANNEL = fxpar(header, 'CHNLNUM') ; number for the wavelength channel
  ;if CHANNEL eq 1 then continue
  AORKEY = fxpar(header, "AORKEY") ; number for the AOR to identify observation
  ;if INSTRUMENT eq 'IRAC' then continue
 ; if CHANNEL ne 1 then continue
;if INSTRUMENT eq 'IRAC' then continue
 close, 11
; openw, 11, "G:\flux_log.txt", /append
  openw, 11, "/media/New\ Volume/flux_log.txt", /append
  if (instrument eq prev_obs.inst and channel eq prev_obs.chan and aorkey eq prev_obs.aor) then begin
    printf, 11, "skipped #", i, " it is same observation"
    continue
  endif
  
  ;for DS9************************
  close, 8
;  openw, 8, "C:\Users\Aaron\Documents\MSU_Research\megan\analysis\DS9\ds9script.txt",/append
   openw, 8, "/media/New\ Volume/ds9script_recheck.txt",/append
  printf, 8, "ds9 -fits \", file(i) + " -zscale \"
  close, 8
  
; determine which BCG is in the IMAGE
; loop through BCG locations to find the nearest 

prev_obs = {aor: aorkey, inst: instrument, chan: channel} 

  min_pos = where(aorkey eq aor_test.aor)



  if min_pos eq -1 then begin
    printf, 11, "can't find aor ", aorkey," skipping i=", i
    continue
  endif


 ra0 = aor_test.ra(min_pos)
 dec0 = aor_test.dec(min_pos)

  
  ;find the nearest xray cluster 
  rarray=dblarr(241) & darray=rarray
  rarray(*)=ra0 & darray(*)=dec0
  distance1 = (xray.ra-rarray)^2.0+(xray.dec-darray)^2
  dis1 = min(distance1)
  distance = (ra-rarray)^2.0+(darray-dec)^2
  dis = min(distance)
  min_xray = where(distance1 eq min(distance1))
  
  ; 2MASS RA and Dec of the BCG
  min_ra = ra(where(distance eq min(distance)))
  min_dec = dec(where(distance eq min(distance)))
  

  ra_xray = xray.ra(min_xray)
  dec_xray = xray.dec(min_xray)
  kpcasec = xray.kpcasec(min_xray)
  ; set the redshift as the nearest to the xray cluster
  redshift = xray.redshift(min_xray)
  radius = 10./.7/kpcasec 
  xray_name = xray.name(min_xray)

; for DS9***************
 ; ds9 = ds9_plot(ra0, dec0, ra_xray, dec_xray, aor_test.name(min_pos))

;continue
; set wavelength, standard error
if INSTRUMENT eq 'IRAC' then begin
  stderr = .05
  ;radius = 10. 
  min_bk = 1.1
  max_bk = 1.3
  if channel eq 1 then begin
    lambda = 3.55
  endif
  
  if channel eq 2 then begin
    lambda = 4.493
  endif
  
  if channel eq 3 then begin
    lambda = 5.731
  endif
  
  if channel eq 4 then begin
    lambda = 7.872
  endif
endif

;set wavelength, standard error, aperture correction
; set radius range based on aperture corrections
if INSTRUMENT eq 'MIPS' then begin
  if channel eq 1 then begin
    aperture = mips_corr(24)
    lambda = 23.7
    stderr = .1
  endif
  
  if channel eq 2 then begin
    aperture = mips_corr(70)
      lambda = 71.
      stderr = .2
  endif
    
  if channel eq 3 then begin
   aperture = mips_corr(160)
   lambda = 156.
    stderr = .2
  endif
    radius = aperture.app
    min_back = aperture.low
    max_back = aperture.high

endif

; name the output file
  aor_name = strcompress(aorkey,/REMOVE_ALL)
  chan_name = strcompress(channel, /REMOVE_ALL)
; This is where all the flux and object information will go
  ;filename1="/media/New\ Volume/fluxes/singleapp/"+aor_name+"_"+INSTRUMENT+"_"+chan_name+".dat"
filename1="/home/Aaron/test_MIPS_filt.dat"
  set_xy ; reset x,y

  steradians_per_sqarcsec = ( !pi / 180. / 3600. )^2.0


  ;output flux to one file with all uncertainties and one to plot the SED
  close, 40
  openw, 40, filename1, /append

; check if MIPS data is filtered
  filt = strpos(file(i), 'mfilt')
  if (filt gt 0 and INSTRUMENT eq 'MIPS') then nfilt='filt' else nfilt=''

; print image header details into the file
  printf, 40, file(i)
  printf, 40, "Object: ", OBJECT
  printf, 40, "CHANNEL: ", INSTRUMENT, " ", strcompress(CHANNEL)
  printf, 40, "AOR: ", strcompress(AORKEY)

; Pixel conversions are different for MIPS and IRAC 
  if (INSTRUMENT eq 'MIPS') then image_scale = abs(fxpar(header, "CDELT2")) * 3600. else image_scale = abs(fxpar(header, "PXSCAL2")) 
  
  if image_scale eq 0 then begin
    printf, 11, "skipped #", i, "the image scale is zero"
    continue
  endif
  
;rad = [20., 25., 30., 35., 40., 45., 50., 55., 60., 64., 70.] 
;for z = 7, 9 do begin ;loop through the list of radii
    
 

  ; if (z eq 0 and INSTRUMENT eq 'MIPS') then z = 0 ;MIPS psf too large to use the small radii
    
 
 ; set radius from text file 
    ;radius = rad(z)
    
   ;radius = 3.0
   ;print, "WARNING HAND INPUT RADIUS" 
   if radius lt 5.0 then radius = 5.0
       
    radius_pixels = radius / image_scale
    radius_pixels = radius_pixels[0]
  printf, 40, "The nearest BCG to the image is ", aor_test.name(min_pos)
  printf, 40, "The radius is: ", radius, " arcseconds"

   printf, 40, "The nearest X-ray cluster is ", xray.name(min_xray)
  if (60*dis gt 1) then begin
     printf, 40, "The x-ray center is greater than 1 arcminute away from the source!"
  endif




min_ra = 15.0*(12.+29./60+46.8/3600)
min_dec = (8.0+0./60+1.2/3600)
print, "WARNING: HAND INPUT RA and DEC"


; convert ra dec to x,y
  adxy,header,min_ra[0],min_dec[0],x0,y0

; make xy arrays xx and yy
  ss = size(image)
  x=findgen(ss(1))
  y=findgen(ss(2))
  xx = x # (y*0.0+1.0)
  yy = (x*0.0+1.0) # y  
  siz = size(xx)
  x0 = x0[0]
  y0= y0[0]
  x0 = make_array(siz(1),siz(2), value = x0)
  y0 = make_array(siz(1),siz(2), value = y0)

  rr = ( (xx-x0)^2 + (yy-y0)^2 )^.5
  rw = where((finite(image) eq 1)*(rr le radius_pixels), npixels)

  if (rw[0] eq -1.0) then begin
    
    printf, 11, "skipped #", i, " the radius wasn't correct"

    continue
  endif
  newimage = image(rw)
  
  flux = total( newimage ) * (image_scale)^2 * steradians_per_sqarcsec * 1.d6 ; convert to Jy
  printf, 40, "Integrated Flux is : ", flux, " Jy"
  if (INSTRUMENT eq 'IRAC') then begin
    min_back = 48.0
    max_back = 64.0
    if min_back le radius(0) then begin
      min_back = 1.05*radius(0)
      max_back = 1.3*radius(0)
    endif
  endif
  ;min_back = 40.
  ;max_back = 80.
  

  ; for MIPS set this based on the radius and aperture corrections 
  r_annulus = where ( (finite(image) eq 1)*(rr gt min_back/image_scale) * (rr le max_back/image_scale), nback)
  if r_annulus(0) eq -1 then continue
  back = image(r_annulus)
  ;print, min(back), max(back)

; filter the background
    nback=float(n_elements(back))
    ave_back=mean(back)
    med_back=median(back)
    sig_back=stddev(back)

; plots of the calculated histograms
  set_plot, 'ps'
;  device, filename="G:\histograms\"+aor_name+"_"+INSTRUMENT+"_"+chan_name+".eps"
  device, filename = "/media/New\ Volume/histograms/"+aor_name+"_"+INSTRUMENT+"_"+chan_name+".eps"
; use histogauss.pro or autohist.pro
; histogram is scaled min to max by +/-3*dispersion
;         binsizes are 1/40 of this for IRAC data
;                      1/4 of this for MIPS

  histogauss, back, hist
  hist_height = hist[0] ; height of the gaussian 
  hist_mean = hist[1]   ; mean of the gaussian
  hist_sd = hist[2]     ; standard deviation of the gaussian
  hist_95 = hist[3]    ; the half-width of the 95% conf. interval of the standard mean
  hist_norm = hist[4]  ; 1/(N-1)*total( (y-mean)/sigma)^2 ) = a measure of normality 
  diff = (hist_mean-med_back)/hist_mean
 printf, 40, "difference between hist and median", diff
 ;med_back = median(back)
  ;offfrac = (med_back -hist_mean)/med_back
  ;if offfrac gt .1 then print, "background off by ", offfrac, file(i)
  ;if hist_height gt 0 then continue
  
  if hist_height eq 0 then begin
    printf, 11, "background histogram failed for i= ", i
     
  end

;                if (INSTRUMENT eq 'IRAC') then binscale=40. else binscale=4.
;    hist=histogram(back, min=med_back-3.0*sig_back, max=med_back+3.0*sig_back, binsize=sig_back/binscale, locations=xhist)
;    !psym=10
;    plot, xhist, hist
;    !psym=0
;    maxhist = xhist( where( hist eq max(hist) )) + 0.5*sig_back/binscale

; try fit to background histogram - get an idea of background dispersion
;  fitg = gaussfit( xhist, hist, acoeff, CHISQ=chisq, NTERMS=3)
;  oplot, xhist,fitg
  if hist_height lt 8 then printf, 40, "background too small"
;  printf, 4, "ACOEFF (normalization, mean, dispersion) = ", strcompress(acoeff)
;  printf, 4, "CHISQ = ", strcompress(chisq)
  
  device, /close
  
; use mean of the gaussian as background estimate
  npix = float(n_elements(rw))
  back = hist_mean & errback=hist_sd

; compute error of the flux

  FLUXCONV = fxpar(header, "FLUXCONV") ; MJY/str per DN/sec
  GAIN = fxpar(header, "GAIN") ; e/DN conversion
  if (INSTRUMENT eq 'MIPS' and CHANNEL ge 2) then GAIN = 7.  


;SOLVE EXPOSURE TIME BY USING MCOV FILES
;print, where(file(i) eq fillname, count)
;print, count
  
  ; need to look at right coverage image for the filtered images
  ;aortext=strcompress(AORKEY,/REMOVE_ALL)
  ;aorcov = where(strpos( covfile, aortext) ne -1)
  covfil = "/home/Aaron/External/External/Spitzer/r4761344/ch1/pbcd/SPITZER_M1_4761344_0000_5_A16804184_mcov.fits"
  ;covimage=mrdfits(covfile(i), 0, covheader)
covimage=mrdfits(covfil, 0, covheader)
  EXPTIME = fxpar(covheader, "EXPTIME") ;exposure time of each BCD


; total fluxes, background and uncertainties in units of electrons
  totflux = total( image(rw)*covimage(rw) )/FLUXCONV * GAIN  * EXPTIME
close, 50
openw,50,'/home/Aaron/70micron.dat' , /append

 ;if hist_height lt 200 then begin
  ;skybit = skyfunc(image, 0, 0, /NAN)
  ;back = skybit
 ;endif
  ;check to make sure covimage is added properly
  totnetflux = total( (newimage - back)*covimage(rw) )/FLUXCONV * GAIN  * EXPTIME
 
  totback = total( back )/FLUXCONV * GAIN * EXPTIME
  toterrback = errback/FLUXCONV * GAIN * EXPTIME
  totavgback = ave_back/FLUXCONV * GAIN * EXPTIME
 
  printf, 40, "Number of electrons (source+background) = ", totflux
  stat_err = sqrt( totflux )  
  printf, 40, "Statistical error (root(N)) = ", stat_err
  
  
  
; compute net flux, error on net flux and standard error for the instruments
  netflux = total( (newimage-back) ) * (image_scale)^2 * steradians_per_sqarcsec * 1.d6 ; convert to Jy
  ;printf, 50, netflux, aor_name
  errflux = sqrt ( abs(totflux) + ((npix^2*toterrback^2)/nback) ) 
  errfrac = errflux/totnetflux ; (S/N)^-1 want 5 sigma upper limit (errfrac < .2)
  snr = totnetflux/errflux
 
  if snr lt 5 then ul = 1 else ul = 0
  if snr lt 5 then lim = 5.0000*(errflux/(mean(covimage(rw)))*FLUXCONV/GAIN/EXPTIME* (image_scale)^2 * steradians_per_sqarcsec * 1.d6) else lim = 0 ;limit in jansky
  if snr eq 0 then coverage = "NOCOV" else coverage = ""
  

  ;if lim gt 0 then stop
  stderr = .2
  stderr = stderr * totnetflux
;        printf, 4, "back = ", back, " Mode of Hist = ", maxhist
        printf, 40, "Average = ", ave_back, " median= ", med_back
        printf, 40, "Net flux = ", netflux, " in Jy or ", totnetflux, " in electrons"
        printf, 40, "Error net flux = ", errflux, " in electrons or ", errfrac*100, " %."
  
; determine if net flux error is larger than standard error
  if (errfrac gt stderr) then printf, 40, "WARNING: Errors are larger than the standard errors!"
  errfrac = errfrac*netflux ; determing the uncertainties in net flux
  printf, 40, "SPECIAL", AORKEY, redshift, netflux, channel, xray_name
  line50 = " "
  line50 = " "+strcompress(xray_name)+" "+strcompress(AORKEY)+" "+strcompress(channel)+" "+strcompress(netflux)+" "+strcompress(ul)+strcompress(lim)+strcompress(coverage)
  
  printf, 50, line50
  stop
; get the uncertainties from the *.unc files & compare
; i  assume image headers are identical

  ;unc_image=mrdfits(uncfile(i), 0, header)


  ;SNR_global = sqrt(total( (image(rw) / unc_image(rw))^2  ) )
  ;printf, 4, "SNR = ", strcompress(SNR_global), "   Error net flux from unc image (Jy) = ", netflux/SNR_global
  ;printf, 4, ""

  
 
  ;create a curve of growth for the fluxes
  ;COG_flux(z)=netflux*1000.
  
  ;endfor
  ;file to search when plotting SED Data
  ; will use this to find the different wavebands for the same objects
  ;lists the cluster name, waveband, net flux and uncertainty on net flux
;  sedfile = "G:\sedfile.txt"
  
  continue
  sedfile = "/media/New\ Volume/sedfile_ALL.txt"
  close, 12
  openw, 12, sedfile, /append
  line = strcompress(xray.name(min_xray))+strcompress(radius)+strcompress(lambda)+strcompress(netflux)+strcompress(AORKEY)
  printf, 12, line
  ;strcompress(aor_test.name(min_pos), /REMOVE_ALL) ; spitzer AOR name
  ;printf, 12, strcompress(xray.name(min_xray), /REMOVE_ALL)    ; ken xray list name
  ;printf, 12, strcompress(ra0, /REMOVE_ALL)                                 ; RA used at the center to make sure it is the right cluster
  ;printf, 12, strcompress(dec0, /REMOVE_ALL)                                ; Dec used at the center to make sure it is the right cluster
  ;printf, 12, strcompress(xray.ra(min_xray), /REMOVE_ALL)                   ; RA from Ken's xray centroid
  ;printf, 12, strcompress(xray.dec(min_xray), /REMOVE_ALL)                  ; Dec from Ken's xray centroid                                         ;
  ;printf, 12, strcompress(redshift, /REMOVE_ALL)                            ; ken xray list redshift
 ; printf, 12, strcompress(lambda, /REMOVE_ALL)                 ; spitzer instrument wavelength
  ;printf, 12,strcompress(netflux, /REMOVE_ALL)                 ; object calculated net flux
  ;printf, 12,strcompress(radius, /REMOVE_ALL)                  ; list the radius for the flux measurement
  ;printf, 12,strcompress(errfrac, /REMOVE_ALL)                 ; error in net flux (may be smaller than systematics)
  close, 12

  set_plot, 'ps'
  ;device, filename="G:\COGS\"+aor_name+"_"+INSTRUMENT+"_"+chan_name+".eps"
  device, filename = "/media/New\ Volume/COGS/"+aor_name+"_"+INSTRUMENT+"_"+chan_name+"_NEW.eps"
 ;   plot, radius, COG_flux
  device, /close

close, 4

 
delvarx, image, xx, yy, R_ANNULUS, rw, y0, x0, rr, covimage, unc_image, /FREE_MEM ; will crash if the image arrays are not deleted
 delvarx, x, y, filename1, header
 
 

endfor
close, 11
close, 41
end
