;determine which is the correct 2mass for the RA and Dec for the BCG

function two_mass, ra, dec, name1

restore, "/home/Aaron/Desktop/Megan/analysis/2MASS/2MASS_reduced.sav"

name = twomass.field01
rat = twomass.field02
dect = twomass.field03




;apertures: 5, 7, 10, 15, 20, 25, 30, 40, 50, 60"

j_flux = [[twomass.field04], [twomass.field10], [twomass.field16], [twomass.field22], [twomass.field28], [twomass.field34], [twomass.field40], [twomass.field46], [twomass.field52], [twomass.field58]]
j_err = [[twomass.field05], [twomass.field11], [twomass.field17], [twomass.field23], [twomass.field29], [twomass.field35], [twomass.field41], [twomass.field47], [twomass.field53], [twomass.field59]]


j_err = double(j_err)
j_flux = double(j_flux)

h_flux = [[twomass.field06], [twomass.field12], [twomass.field18], [twomass.field24], [twomass.field30], [twomass.field36], [twomass.field42], [twomass.field48], [twomass.field54], [twomass.field60]]
h_err = [[twomass.field07], [twomass.field13], [twomass.field19], [twomass.field25], [twomass.field31], [twomass.field37], [twomass.field43], [twomass.field49], [twomass.field55], [twomass.field61]]

h_flux = double(h_flux)
h_err = double(h_err)

k_flux = [[twomass.field08], [twomass.field14], [twomass.field20], [twomass.field26], [twomass.field32], [twomass.field38], [twomass.field44], [twomass.field50], [twomass.field56], [twomass.field62]]
k_err = [[twomass.field09], [twomass.field15], [twomass.field21], [twomass.field27], [twomass.field33], [twomass.field39], [twomass.field45], [twomass.field51], [twomass.field57], [twomass.field63]]

k_flux = double(k_flux)
k_err = double(k_err)

;convert the magnitudes into fluxes (in mJy)

j_err = (1.45974E6)*EXP(-0.921034*j_flux)*j_err ; j_flux and j_err are in MAGNITUDES
j_flux = 10.^((23.9-(j_flux+0.9))/2.5)/1000. ; j_flux NOW converted to mJy

h_err = 9.46838E5*EXP(-0.921034*h_flux)*h_err
h_flux = 10.^((23.9-(h_flux+1.37))/2.5)/1000.

k_err =6.14152E5*EXP(-0.921034*k_flux)*k_err
k_flux = 10.^((23.9-(k_flux+1.84))/2.5)/1000.



distance = sqrt((ra-rat)^2 + (dec-dect)^2) 

min_pos = where(distance eq min(distance))
;min_pos = where(name eq name1)
min_pos = min_pos(0)

;for galex interpolation if no 2MASS point for GALEX observation
emptyset = {ra: 0}
if min_pos eq -1 then return, emptyset 




;chooses the correct object for 2034
if name(min_pos) eq "ABELL_2034" then begin

    print, min_pos
   if name(min_pos-1) eq name(min_pos) then min_pos = min_pos-1
   if name(min_pos+1) eq name(min_pos) then min_pos = min_pos + 1
    print, min_pos
    min_pos = min_pos
    print, min_pos
endif
;print all of the information to the file

;print, name(min_pos)

;change to include all the radii
mass = {name: name(min_pos), ra: rat(min_pos), dec: dect(min_pos), j: j_flux[min_pos,*], jerr: j_err[min_pos,*],$
 h: h_flux[min_pos, *], herr: h_err[min_pos,*], k: k_flux[min_pos,*], kerr: k_err[min_pos,*]}
 
return, mass 

end